module clipboard-agent

go 1.14

require (
	github.com/atotto/clipboard v0.1.2
	github.com/gojektech/heimdall v5.0.2+incompatible
	github.com/gojektech/valkyrie v0.0.0-20190210220504-8f62c1e7ba45 // indirect
	github.com/ilyakaznacheev/cleanenv v1.2.4
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.6.0
)
