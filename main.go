package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/atotto/clipboard"
	"github.com/gojektech/heimdall"
	"github.com/gojektech/heimdall/httpclient"
	"github.com/ilyakaznacheev/cleanenv"
	log "github.com/sirupsen/logrus"
)

var textOld string

// Config is a application configuration structure
type Config struct {
	Scraper struct {
		Domains  string `yaml:"domains" env:"SCR_DOMAINS" env-description:"Domain Whitelist"`
		Interval string `yaml:"interval" env:"SCR_INTERVAL" env-description:"Interval in Seconds" env-default:"5"`
	} `yaml:"scraper"`
	Credentials struct {
		Username string `yaml:"username" env:"CRD_USERNAME" env-description:"Login Username"`
		Password string `yaml:"password" env:"CRD_PASSWORD" env-description:"Login Password"`
		Access   string `yaml:"access" env:"CRD_ACCESS" env-description:"Access Token"`
	} `yaml:"credentials"`
	Server struct {
		Host     string `yaml:"host" env:"SRV_HOST" env-description:"Server host" env-default:"127.0.0.1"`
		Timeout  int    `yaml:"timeout" env:"SRV_TIMEOUT" env-description:"Server timeout" env-default:"2"`
		Exponent int    `yaml:"exponent" env:"SRV_EXPONENT" env-description:"Server exponent" env-default:"2"`
		Retry    int    `yaml:"retry" env:"SRV_RETRY" env-description:"Server Retry" env-default:"4"`
	} `yaml:"server"`
	Logging struct {
		Level  string `yaml:"level" env:"LOG_LEVEL" env-description:"trace|debug|info|warning|error|fatal" env-default:"warning"`
		Caller string `yaml:"caller" env:"LOG_CALLER" env-description:"true|false" env-default:"false"`
	} `yaml:"logging"`
}

// LoginResponse contains the Bearer Token used to send requests
type LoginResponse struct {
	TokenType    string `json:"token_type"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json"refresh_token"`
	Username     string `json"username"`
}

func initConfig(conf Config) (Config, error) {
	err := cleanenv.ReadConfig("config.yml", &conf)
	if err != nil {
		return conf, err
	}
	return conf, nil
}

func initLogging(conf Config) {
	// Check Report Caller
	if strings.ToLower(conf.Logging.Caller) == "true" {
		log.SetReportCaller(true)
	}
	// Check Log Level
	level := strings.ToLower(conf.Logging.Level)
	switch level {
	case "trace":
		log.SetLevel(log.TraceLevel)
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warning":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.FatalLevel)
	}
}

func login(cfg Config, cl heimdall.Client) string {
	// Post the Link to the Backend
	urlLogin := fmt.Sprintf("%s/auth/login", cfg.Server.Host)

	// Create Params: link, user_id
	requestBody, err := json.Marshal(map[string]string{
		"username": cfg.Credentials.Username,
		"password": cfg.Credentials.Password,
	})
	if err != nil {
		return err.Error()
	}

	// Prepare the Request
	req, err := http.NewRequest(http.MethodPost, urlLogin, bytes.NewBuffer(requestBody))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err.Error()
	}

	// Do the Request
	var lr LoginResponse
	res, err := cl.Do(req)
	if err != nil {
		return err.Error()
	}
	err = json.NewDecoder(res.Body).Decode(&lr)
	if err != nil {
		return err.Error()
	}
	return lr.AccessToken
}

func checkClipboardContent(cfg Config, text string) (bool, error) {
	// Read Clipbopad
	log.Debug(text)

	// Check for URL
	u, err := url.Parse(text)
	if err != nil {
		return false, err
	}

	// Debug URL Componentes
	log.Debug(u.Host)               // preview.redd.it
	log.Debug(u.Path)               // areucgxqu9g41.jpg
	log.Debug(filepath.Ext(u.Path)) // jpg
	log.Debug(u.RawQuery)           // width=640&crop=smart&auto=webp&s=03bc67b1de51f8286641d8405a2a9ebcbc094a2c

	// Get all Whitelisted Domains
	domains := strings.Split(cfg.Scraper.Domains, ",")
	b := false
	for i := 0; i < len(domains); i++ {
		b = strings.Contains(u.Host, domains[i]) // true
		if b {
			break
		}
	}

	// Check if URL is in whitelist
	return b, nil
}

func initHTTPClient(cfg Config) heimdall.Client {
	// First set a backoff mechanism. Exponential Backoff increases the backoff at a exponential rate
	initalTimeout := 2 * time.Millisecond          // Inital timeout
	maxTimeout := 10 * time.Millisecond            // Max time out
	exponentFactor := float64(cfg.Server.Exponent) // Multiplier
	maximumJitterInterval := 2 * time.Millisecond  // Max jitter interval. It must be more than 1*time.Millisecond

	backoff := heimdall.NewExponentialBackoff(initalTimeout, maxTimeout, exponentFactor, maximumJitterInterval)

	// Create a new retry mechanism with the backoff
	retrier := heimdall.NewRetrier(backoff)

	timeout := time.Duration(cfg.Server.Timeout) * 1000 * time.Millisecond
	// Create a new client, sets the retry mechanism, and the number of times you would like to retry
	client := httpclient.NewClient(
		httpclient.WithHTTPTimeout(timeout),
		httpclient.WithRetrier(retrier),
		httpclient.WithRetryCount(cfg.Server.Retry),
	)
	return client
}

func sendRequestToServer(cfg Config, cl heimdall.Client, text string) error {
	// Post the Link to the Backend
	urlAdd := fmt.Sprintf("%s/api/add_meme", cfg.Server.Host)

	// Create Params: link, user_id
	requestBody, err := json.Marshal(map[string]string{
		"url":     text,
		"user_id": cfg.Credentials.Username,
	})
	if err != nil {
		return err
	}

	// Prepare the Request
	req, err := http.NewRequest(http.MethodPost, urlAdd, bytes.NewBuffer(requestBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer: "+cfg.Credentials.Access)
	if err != nil {
		return err
	}

	// Do the Request
	res, err := cl.Do(req)
	if err != nil {
		return err
	}
	log.Debug(res.Status)

	return nil
}

func mainRoutine(cfg Config, cl heimdall.Client) {
	// Check Clipboard Content
	text, _ := clipboard.ReadAll()
	if text != textOld {
		textOld = text
		log.Debug(text)
		whitelisted, err := checkClipboardContent(cfg, text)
		if err != nil { // Not a Link
			log.Warning(err.Error())
		}
		if whitelisted {
			log.Debug(whitelisted)
			err = sendRequestToServer(cfg, cl, text)
			if err != nil {
				log.Error(err.Error())
			}
		}
	}
}

func startPolling(cfg Config, cl heimdall.Client) {
	i, err := strconv.Atoi(cfg.Scraper.Interval)
	if err != nil {
		log.Error(err.Error())
		i = 5
	}
	d := time.Duration(i)
	for {
		time.Sleep(d * time.Second)
		go mainRoutine(cfg, cl)
	}
}

func main() {
	// Read Config
	var cfg Config
	cfg, err := initConfig(cfg)
	if err != nil {
		log.Warn(err.Error())
	}

	// Init Logging
	initLogging(cfg)
	log.Debug(cfg)

	// Init HTTP Client
	cl := initHTTPClient(cfg)

	// Login
	token := login(cfg, cl)
	log.Debug(token)
	cfg.Credentials.Access = token

	// Main Routine
	go startPolling(cfg, cl)

	// Wait here until CTRL-C or other term signal is received.
	log.Debug("Clipboard Agent is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}
